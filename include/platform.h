#include "bn_core.h"
#include "bn_sprite_ptr.h"
#include "bn_camera_actions.h"
#include <bn_fixed_rect.h>

class Platform{
    public:
        Platform(bn::sprite_ptr newSprite, bn::fixed_point box, bn::camera_ptr& camera);
        bn::sprite_ptr sprite;
        bn::fixed_rect collision_box = bn::fixed_rect();
};