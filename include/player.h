#include "bn_core.h"
#include "bn_keypad.h"
#include "bn_sprite_ptr.h"
#include "bn_sprite_items_test.h"
#include "bn_sprite_animate_actions.h"
#include <bn_fixed_rect.h>


class playerchar{
    public:
        playerchar(bn::camera_ptr& camera);
        bn::sprite_ptr sprite = bn::sprite_items::test.create_sprite(0, 0);
        bn::fixed_rect collision_box = bn::fixed_rect();
        bn::fixed_rect feet = bn::fixed_rect();
        bn::fixed_point velocity = bn::fixed_point();
        bn::sprite_animate_action<6> action = bn::create_sprite_animate_action_forever(sprite, 8, bn::sprite_items::test.tiles_item(), 3, 4, 5, 6, 7, 8);
        const int SPEED = 3;
        uint16_t currentAnim = -1;
        
        bool onGround = false;
        bn::fixed_rect platform = bn::fixed_rect();
        bool collide(bn::fixed_rect rect);

        void update();
        bn::sprite_animate_action<6> getAnim(uint16_t anim);
};