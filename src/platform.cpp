#include "platform.h"
#include "bn_core.h"
#include "bn_sprite_ptr.h"
#include "bn_camera_actions.h"
#include <bn_fixed_rect.h>

Platform::Platform(bn::sprite_ptr newSprite, bn::fixed_point box, bn::camera_ptr& camera) : sprite(newSprite)
{
    this->sprite = newSprite;
    this->sprite.set_camera(camera);
    this->collision_box = bn::fixed_rect(this->sprite.x(), this->sprite.y(), box.x(), box.y());
}