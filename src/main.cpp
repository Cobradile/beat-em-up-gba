#include "bn_core.h"
#include "bn_keypad.h"
#include "bn_sprite_ptr.h"
#include "bn_regular_bg_ptr.h"
#include "bn_camera_actions.h"
#include "bn_regular_bg_items_map.h"
#include "bn_sprite_items_platform.h"

#include "player.h"
#include "platform.h"

namespace
{

    void camera_scene(bn::camera_ptr& camera)
    {
        if(bn::keypad::left_held())
        {
            camera.set_x(camera.x() - 1);
        }
        else if(bn::keypad::right_held())
        {
            camera.set_x(camera.x() + 1);
        }

        if(bn::keypad::up_held())
        {
            camera.set_y(camera.y() - 1);
        }
        else if(bn::keypad::down_held())
        {
            camera.set_y(camera.y() + 1);
        }
        // while(! bn::keypad::start_pressed())
        // {
        //     if(bn::keypad::left_held())
        //     {
        //         camera.set_x(camera.x() - 1);
        //     }
        //     else if(bn::keypad::right_held())
        //     {
        //         camera.set_x(camera.x() + 1);
        //     }

        //     if(bn::keypad::up_held())
        //     {
        //         camera.set_y(camera.y() - 1);
        //     }
        //     else if(bn::keypad::down_held())
        //     {
        //         camera.set_y(camera.y() + 1);
        //     }
        //     bn::core::update();
        // }

        // camera.set_position(0, 0);
    }
}

int main()
{
    bn::core::init();
    bn::regular_bg_ptr regular_bg = bn::regular_bg_items::map.create_bg(0, 0);
    //bn::fixed_rect collision_box = bn::fixed_rect(0, 100, 100, 100);

    bn::camera_ptr camera = bn::camera_ptr::create(0, 0);
    regular_bg.set_camera(camera);
    playerchar* player = new playerchar(camera);
    Platform* platforms[3];
    platforms[0] = new Platform(bn::sprite_items::platform.create_sprite(0, 60), bn::fixed_point(60, 30), camera);
    platforms[1] = new Platform(bn::sprite_items::platform.create_sprite(60, 60), bn::fixed_point(60, 30), camera);
    platforms[2] = new Platform(bn::sprite_items::platform.create_sprite(120, 60), bn::fixed_point(60, 30), camera);
    //(bn::sprite_ptr newSprite, bn::fixed_point pos, bn::fixed_point box, bn::camera_ptr& camera)

    while(true)
    {
        //camera_scene(camera);
        player->update();
        for(Platform* platform : platforms) player->collide(platform->collision_box);
        bn::core::update();
    }
}
