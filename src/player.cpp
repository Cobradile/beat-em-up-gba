#include "player.h"

#include "bn_core.h"
#include "bn_keypad.h"
#include "bn_sprite_ptr.h"
#include "bn_camera_actions.h"
#include "bn_sprite_animate_actions.h"
#include <bn_fixed_rect.h>

constexpr uint16_t WALK[] = {3, 4, 5, 6, 7, 8};
constexpr uint16_t IDLE[] = {0, 1, 2};

playerchar::playerchar(bn::camera_ptr& camera)
{
    this->sprite.set_camera(camera);
    this->collision_box = bn::fixed_rect(sprite.x(), sprite.y(), 10, 30);
    this->feet = bn::fixed_rect(sprite.x(), sprite.y() + 30, 10, 1);
}

void playerchar::update()
{
    if (!this->feet.intersects(this->platform))
    {
        if (this->velocity.y() <= 9) this->velocity.set_y(this->velocity.y() + 0.5);
    }
    else if (bn::keypad::up_pressed())
    {
        this->onGround = false;
        this->velocity.set_y(-8);
    }
    else this->velocity.set_y(0);

    if(bn::keypad::left_held())
    {
        this->velocity.set_x(-this->SPEED);
        this->sprite.set_horizontal_flip(true);
        if (currentAnim != 1) this->action = this->getAnim(1);
    }
    else if (bn::keypad::right_held())
    {
        this->velocity.set_x(this->SPEED);
        this->sprite.set_horizontal_flip(false);
        if (currentAnim != 1) this->action = this->getAnim(1);
    }
    else
    {
        this->velocity.set_x(0);
        if (currentAnim != 0) this->action = this->getAnim(0);
    }

    this->sprite.set_position(this->sprite.x() + this->velocity.x(), this->sprite.y() + this->velocity.y());
    this->collision_box.set_position(sprite.position());
    this->feet.set_position(sprite.x(), sprite.y() + 30);
    this->action.update();
}

bool playerchar::collide(bn::fixed_rect rect)
{
    if (this->feet.intersects(rect))
    {
        this->onGround = true;
        this->platform = rect;
    }
    return this->onGround;
}

bn::sprite_animate_action<6> playerchar::getAnim(uint16_t anim)
{
    constexpr int MAX_FRAMES = 6;
    currentAnim = anim;
    switch(anim)
    {
        case 1:
            return bn::sprite_animate_action<MAX_FRAMES>::forever(this->sprite, 4, bn::sprite_items::test.tiles_item(), WALK); 
            break;
        default:
            return bn::sprite_animate_action<MAX_FRAMES>::forever(this->sprite, 32, bn::sprite_items::test.tiles_item(), IDLE);
            break; 
    }
}